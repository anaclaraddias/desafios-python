
class romano:

    def __init__(self):
        
        self.romano = input("Digite o numero romano --> ").lower()
        self.list = list(self.romano)

        self.quant1000 = 0
        self.quant500 = 0
        self.quant100 = 0
        self.quant50 = 0
        self.quant10 = 0
        self.quant5 = 0
        self.quant1 = 0

        self.numI = False
        self.numX = False
        self.numC = False
        self.soma = 0

        self.fat()


    def fat(self):
        
        for i,e in enumerate(self.list):

            if e == "i" and i != len(self.list) - 1 or e == "x" and i != len(self.list) - 1 or e == "c" and i != len(self.list) - 1:

                self.ver()

            if e == "m":

                self.quant1000 += 1 

            elif e == "d":

                self.quant500 += 1

            elif e == "c":

                self.quant100 += 1 

            elif e == "l":

                self.quant50 += 1 

            elif e == "x":

                self.quant10 += 1 

            elif e == "v":

                self.quant5 += 1 

            elif e == "i":

                self.quant1 += 1 

        self.sum()


    def ver(self):

        testI = False
        testX = False
        testC = False

        for i,e in enumerate(self.list):

            if e == "i" and i != len(self.list) - 1:

                indx = i
                testI = True #LXXXVII --> I retornando como true 

                break

            if e == "x" and i != len(self.list) - 1:

                indx = i
                testX = True

                

            if e == "c" and i != len(self.list) - 1:

                indx = i
                testC = True

                break





        if testI:

            if self.list[indx + 1] == "v":

                self.numI = True

            elif self.list[indx + 1] == "x":

                self.numI = True

            elif self.list[indx + 1] == "l":

                self.numI = True

            elif self.list[indx + 1] == "c":

                self.numI = True

            elif self.list[indx + 1] == "d":

                self.numI = True

            elif self.list[indx + 1] == "m":

                self.numI = True


        if testX:

            if self.list[indx + 1] == "l":

                self.numX = True

            elif self.list[indx + 1] == "c":

                self.numX = True

            elif self.list[indx + 1] == "d":

                self.numX = True


        if testC:

            if self.list[indx + 1] == "d":

                self.numC = True

            elif self.list[indx + 1] == "m":

                self.numC = True

            elif self.list[indx + 1] == "i" :

                self.numI = True

            elif self.list[indx - 1] == "x" :

                self.numX = True

    


    def sum(self):

        self.quant1000 = self.quant1000 * 1000
        self.quant500 = self.quant500 * 500
        self.quant100 = self.quant100 * 100
        self.quant50 = self.quant50 * 50
        self.quant10 = self.quant10 * 10
        self.quant5 = self.quant5 * 5
        self.quant1 = self.quant1 * 1

        self.soma = self.quant1000 + self.quant500 + self.quant100 + self.quant50 + self.quant10 + self.quant5 + self.quant1

        if self.numI:

            self.soma = self.soma - 2

        if self.numX:

            self.soma = self.soma - 20

        if self.numC:

            self.soma = self.soma - 200

        print(f"O número romano {self.romano} corresponde a: {self.soma}")


a = romano()