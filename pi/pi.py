
import os
import requests

class calc:

    def __init__(self):

        os.system("clear")

        print("Carregando π...")
        
        num = "http://www.geom.uiuc.edu/~huberty/math5337/groupe/digits.html"

        retorno = requests.get(num)

        with open("decimais.txt", "w") as arquivo:

            arquivo.write(retorno.text)

            os.system("clear")

            print("Carregado!\n")


        self.leitura()

    
    def leitura(self):

        with open("decimais.txt", "r") as arquivo:

            num = [e[0:len(e) - 1] for e in arquivo.readlines()]

            self.pi = "".join([e for e in num[12:1296]])
            self.pi = self.pi.split(".")
            self.pi = self.pi[1]


        with open("decimais.txt", "w") as arquivo:

            arquivo.write(self.pi)

        
        self.prog()

    
    def prog(self):

            res = int(input("Quantas casas decimais devem ser analizadas?\n --> "))

            self.pi = self.pi[0:res]

            self.re = False

            self.primo = []
            self.seq = []
            self.seqM = []
            cont = 0

            print(f"\nnúmero pi com as casas solicitadas: 3.{self.pi}\n")

            for i,e in enumerate(self.pi):

                cont +=1

                num = int(e)
                
                if num % 2 == 0 and num != 2:

                    self.re = False
                
                elif num % 3 == 0 and num != 3:

                    self.re = False

                elif num % 5 == 0 and num != 5:

                    self.re == False

                elif num % 7 == 0 and num != 7:

                    self.re = False

                elif num % 11 == 0 and num != 11:

                    self.re = False
 
                elif num != 1:

                    if num not in self.primo:

                        self.primo.append(num)


                if cont > 1:

                    if num in self.primo:

                        self.seq.append(num)

                        if int(self.pi[i-1]) in self.primo:

                            if int(self.pi[i-1]) not in self.seq:
                                self.seq.append(int(self.pi[i-1]))

                        if len(self.seq) > len(self.seqM):

                            self.seqM = self.seq

                    else:

                        self.seq = []

            print(f"Os números primos presentes nas casas solicitadas são:\n{self.primo}")
            print(f"\nMaior sequência de números primos:\n{self.seqM}")
            


a = calc()
